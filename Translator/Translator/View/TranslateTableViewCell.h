//
//  TranslateTableViewCell.h
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TranslateModel;

IB_DESIGNABLE
@interface TranslateTableViewCell : UITableViewCell

-(void)fillData:(TranslateModel*)translateModel;

@end
