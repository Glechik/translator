//
//  TranslateRequest.h
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TranslatingDelegate;
@class TranslateModel;

@interface TranslateRequest : NSOperation

-(instancetype)initWithModel:(TranslateModel*)translateModel delegate:(id<TranslatingDelegate>)delegate;

@end
