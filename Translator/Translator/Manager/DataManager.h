//
//  DataManager.h
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TranslatingDelegate;
@class TranslateModel;

@interface DataManager : NSObject

-(instancetype)initWithDelegate:(id<TranslatingDelegate>)delegate;

-(NSArray<TranslateModel*>*)getAllWords;
-(TranslateModel*)addWord:(NSString*)word index:(NSInteger)index;
-(void)clearAll;

@end
