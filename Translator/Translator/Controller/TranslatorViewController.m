//
//  TranslatorViewController.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "TranslatorViewController.h"
#import "DataManager.h"
#import "TranslateModel.h"
#import "TranslateTableViewCell.h"

@interface TranslatorViewController() <TranslatingDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end

@implementation TranslatorViewController
{
    __weak IBOutlet UITableView *mTableView;
    __weak IBOutlet UITextField *mInputTextField;
    __weak IBOutlet UIBarButtonItem *mAddButton;
    IBOutlet UIToolbar *mToolbar;

    NSMutableArray<TranslateModel*>* mTranslateModel;
    DataManager* mDataManager;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    mDataManager = [[DataManager alloc] initWithDelegate:self];
    mTranslateModel = [[NSMutableArray alloc] initWithArray:[mDataManager getAllWords]];

    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];

    mTableView.contentInset = UIEdgeInsetsMake(0, 0, mToolbar.frame.size.height, 0);
    mTableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, mToolbar.frame.size.height, 0);
}

-(UIView *)inputAccessoryView
{
    return mToolbar;
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

#pragma mark - Actions

- (IBAction)clearButtonClick:(id)sender
{
    [mDataManager clearAll];

    mTranslateModel = [NSMutableArray new];
    [mTableView reloadData];
}

- (IBAction)addButtonClick:(id)sender
{
    TranslateModel* newTranslateModel = [mDataManager addWord:mInputTextField.text index:mTranslateModel.count];

    [mTranslateModel insertObject:newTranslateModel atIndex:0];
    [mTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [mTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

    mInputTextField.text = @"";
    mAddButton.enabled = NO;
}

#pragma mark - Textfield Delegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // follow to rules - only one word without special symbols

    NSCharacterSet *charactersToBlock = [[NSCharacterSet letterCharacterSet] invertedSet];
    return ([string rangeOfCharacterFromSet:charactersToBlock].location == NSNotFound);
}

- (IBAction)textFieldDidChange:(UITextField *)textField
{
    mAddButton.enabled = ![textField.text isEqualToString:@""];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self addButtonClick:nil];
    return YES;
}

#pragma mark - Translating Delegate

-(void)translatedModel:(TranslateModel *)translateModel
{
    NSUInteger updateRow = mTranslateModel.count - (NSUInteger)translateModel.index - 1;
    [mTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:updateRow inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - UITableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mTranslateModel.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TranslateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TranslateTableViewCell class]) forIndexPath:indexPath];
    [cell fillData:mTranslateModel[indexPath.row]];
    return cell;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [mInputTextField resignFirstResponder];
}

@end
