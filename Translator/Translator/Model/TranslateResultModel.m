//
//  TranslateResultModel.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "TranslateResultModel.h"

@implementation TranslateResultModel
{
    NSDictionary *mProxy;
}

-(instancetype)initWithData:(id)data
{
    self = [super init];

    if([data isKindOfClass:[NSDictionary class]])
    {
        mProxy = data;
    }

    return self;
}

-(NSString *)englishWord
{
    if (mProxy)
    {
        NSArray* arrWords = [mProxy objectForKey:@"text"];
        return arrWords.firstObject;
    }
    else
    {
        return @"?";
    }
}

@end
