//
//  TranslateResultModel.h
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TranslateResultModel : NSObject

-(instancetype)initWithData:(id)data;

@property (nonatomic, readonly) NSString* englishWord;

@end