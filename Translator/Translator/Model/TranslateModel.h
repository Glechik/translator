//
//  TranslateModel.h
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef NS_ENUM(int16_t, LoadState)
{
    LoadStateProcess = 0,
    LoadStateComplete = 1,
    LoadStateError = 2
};

@interface TranslateModel : NSManagedObject

@property (nonatomic) int64_t index;

@property (nonatomic) NSString* firstWord;
@property (nonatomic) NSString* secondWord;

@property (nonatomic) LoadState loadState;

-(instancetype)initWithManagedObjectContext:(NSManagedObjectContext*)moc firstWord:(NSString*)firstWord index:(int64_t)index;

@end

#pragma mark - Translating Delegate

@protocol TranslatingDelegate <NSObject>

@optional

-(void)translatedModel:(TranslateModel*)translateModel;
-(void)translatedWord:(NSString*)translatedWord forModel:(TranslateModel*)translateModel;
-(void)errorTranslate:(TranslateModel*)translateModel;

@end