//
//  NSURLComponents+Helper.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "NSURLComponents+Helper.h"

@implementation NSURLComponents (Helper)

-(void)setParam:(NSString*)param value:(NSString*)value
{
    NSURLQueryItem* item = [[NSURLQueryItem alloc] initWithName:param value:value];
    NSMutableArray<NSURLQueryItem *> *queryItems = [NSMutableArray arrayWithArray:self.queryItems];
    [queryItems addObject:item];
    self.queryItems = queryItems;
}

@end
