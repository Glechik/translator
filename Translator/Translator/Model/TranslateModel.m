//
//  TranslateModel.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "TranslateModel.h"

@implementation TranslateModel

@dynamic index, firstWord, secondWord, loadState;

-(instancetype)initWithManagedObjectContext:(NSManagedObjectContext*)moc firstWord:(NSString*)firstWord index:(int64_t)index
{
    self = [super initWithEntity:[NSEntityDescription entityForName:@"TranslateModel" inManagedObjectContext:moc] insertIntoManagedObjectContext:moc];

    self.index = index;

    self.firstWord = firstWord;
    self.secondWord = @"";

    self.loadState = LoadStateProcess;

    return self;
}

@end
