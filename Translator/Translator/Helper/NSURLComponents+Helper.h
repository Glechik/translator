//
//  NSURLComponents+Helper.h
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLComponents (Helper)

-(void)setParam:(NSString*)param value:(NSString*)value;

@end
