//
//  TranslateTableViewCell.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "TranslateTableViewCell.h"
#import "TranslateModel.h"

@implementation TranslateTableViewCell
{
    __weak IBOutlet UIView *mSeparateView;
    __weak IBOutlet NSLayoutConstraint *mSeparateViewWidthConstraint;
    __weak IBOutlet UIView *mBorderView;


    __weak IBOutlet UILabel *mFirstLabel;
    __weak IBOutlet UILabel *mSecondLabel;

    __weak IBOutlet UIActivityIndicatorView *mActivitiyIndicatorView;
}

-(void)awakeFromNib
{
    [self configurateInterface];
}

-(void)prepareForInterfaceBuilder
{
    [self configurateInterface];
}

-(void)configurateInterface
{
    CGColorRef borderColor = mSeparateView.backgroundColor.CGColor;
    CGFloat borderWidth = mSeparateViewWidthConstraint.constant;

    mBorderView.layer.cornerRadius = 8;
    mBorderView.layer.borderColor = borderColor;
    mBorderView.layer.borderWidth = borderWidth;
}

-(void)fillData:(TranslateModel*)translateModel
{
    mFirstLabel.text = translateModel.firstWord;
    mSecondLabel.text = translateModel.secondWord;

    mSecondLabel.hidden = translateModel.loadState == LoadStateProcess;
    mActivitiyIndicatorView.hidden = translateModel.loadState != LoadStateProcess;

    mSecondLabel.textColor = translateModel.loadState == LoadStateError ? [UIColor redColor] : [UIColor darkTextColor];

    if (translateModel.loadState == LoadStateProcess)
    {
        [mActivitiyIndicatorView startAnimating];
    }
    else
    {
        [mActivitiyIndicatorView stopAnimating];
    }
}

@end
