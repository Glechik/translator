//
//  AppDelegate.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "AppDelegate.h"
#import "TranslateModel.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    return YES;
}

+(NSString *)translateKey
{
    return @"trnsl.1.1.20160506T143642Z.7756823cb5e36140.987b947b2262a7cedd60db555bd50603daaf5531";
}

+(NSString*)translateHost
{
    return @"https://translate.yandex.net";
}

+(NSString*)translateApiPrefix
{
    return @"api/v1.5/tr.json/translate";
}


@end
