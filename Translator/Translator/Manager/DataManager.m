//
//  DataManager.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "DataManager.h"
#import "TranslateModel.h"
#import <CoreData/CoreData.h>
#import "TranslateRequest.h"
#import "Reachability.h"

@interface DataManager() <TranslatingDelegate>

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation DataManager
{
    __weak id<TranslatingDelegate> mDelegate;
    NSOperationQueue* mTranslateQueue;
}

-(instancetype)initWithDelegate:(id<TranslatingDelegate>)delegate
{
    self = [super init];

    mDelegate = delegate;
    mTranslateQueue = [NSOperationQueue new];
    mTranslateQueue.suspended = YES;
    [self checkInternetConnecton];

    return self;
}

#pragma mark - Data methods

-(NSArray<TranslateModel *> *)getAllWords
{
    // load and return all word

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([TranslateModel class])];
    NSSortDescriptor *indexSort = [[NSSortDescriptor alloc] initWithKey:@"index" ascending:NO];

    request.sortDescriptors = @[indexSort];

    NSError *error = nil;
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (error != nil)
    {
        NSLog(@"Error fetching %@ objects: %@\n%@", NSStringFromClass([TranslateModel class]),[error localizedDescription], [error userInfo]);
        abort();
    }

    // translating word without translate

    for (TranslateModel* translateModel in results)
    {
        if (translateModel.loadState == LoadStateProcess)
        {
            [self startTranslate:translateModel];
        }
    }

    return results;
}


-(TranslateModel*)addWord:(NSString *)word index:(NSInteger)index
{
    TranslateModel* newWord = [[TranslateModel alloc] initWithManagedObjectContext:self.managedObjectContext firstWord:word index:index];

    [self saveContext];

    [self startTranslate:newWord];

    return newWord;
}

-(void)startTranslate:(TranslateModel*)translateModel
{
    TranslateRequest *translateRequest = [[TranslateRequest alloc] initWithModel:translateModel delegate:self];
    [mTranslateQueue addOperation:translateRequest];
}

-(void)clearAll
{
    [self clearTranslateModelEntity];
}

#pragma mark - Internet Connection

-(void)checkInternetConnecton
{
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];

    reach.reachableBlock = ^(Reachability*reach)
    {
        mTranslateQueue.suspended = NO;
    };

    reach.unreachableBlock = ^(Reachability*reach)
    {
        mTranslateQueue.suspended = YES;
    };

    [reach startNotifier];
}

#pragma mark - Translating Delegate

-(void)translatedModel:(TranslateModel *)translateModel
{
    [self saveContext];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // update to UI
        [mDelegate translatedModel:translateModel];
    }];
}

-(void)translatedWord:(NSString *)translatedWord forModel:(TranslateModel *)translateModel
{
    translateModel.secondWord = translatedWord;
    translateModel.loadState = LoadStateComplete;

    [self saveContext];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // update to UI
        [mDelegate translatedModel:translateModel];
    }];
}

-(void)errorTranslate:(TranslateModel *)translateModel
{
    translateModel.secondWord = @"Error";
    translateModel.loadState = LoadStateError;

    [self saveContext];
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        // update to UI
        [mDelegate translatedModel:translateModel];
    }];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Translator" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    // Create the coordinator and store

    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Translator.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        #ifdef DEBUG
            abort();
        #endif
    }

    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil){
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            NSLog(@"Save unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

-(void)clearTranslateModelEntity
{
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([TranslateModel class])];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];

    NSError *deleteError = nil;
    [self.persistentStoreCoordinator executeRequest:delete withContext:self.managedObjectContext error:&deleteError];
    if(deleteError)
    {
         NSLog(@"Clear translate entity error: %@", [deleteError localizedDescription]);
    }
}

@end
