//
//  TranslateRequest.m
//  Translator
//
//  Created by Dmitry Savchenko on 06.05.16.
//  Copyright © 2016 Dmitry Savchenko. All rights reserved.
//

#import "TranslateRequest.h"
#import "TranslateModel.h"
#import "TranslatorViewController.h"
#import "AppDelegate.h"
#import "NSURLComponents+Helper.h"
#import "TranslateResultModel.h"

@implementation TranslateRequest
{
    __weak id<TranslatingDelegate> mTranslatingDelegate;
    __weak TranslateModel* mTranslateModel;
    BOOL isFinished;
    BOOL isExecuting;
    NSURLComponents* mComponents;
}

-(instancetype)initWithModel:(TranslateModel *)translateModel delegate:(id<TranslatingDelegate>)delegate
{
    self = [super init];

    mTranslateModel = translateModel;
    mTranslatingDelegate = delegate;
    isFinished = NO;
    isExecuting = NO;

    mComponents = [NSURLComponents new];

    return self;
}

-(void)main
{
    isExecuting = YES;

    [mComponents setPercentEncodedPath:[NSString stringWithFormat:@"%@/%@", [AppDelegate translateHost], [AppDelegate translateApiPrefix]]];
    [mComponents setParam:@"key" value:[AppDelegate translateKey]];
    [mComponents setParam:@"text" value:mTranslateModel.firstWord];
    [mComponents setParam:@"lang" value:@"ru-en"];
    NSLog(@"GET Request: %@", mComponents.URL);

    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:mComponents.URL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    [request setHTTPMethod:@"GET"];

    NSHTTPURLResponse* response;
    NSError* requestError;
    NSData* result = [NSURLConnection  sendSynchronousRequest:request
                                            returningResponse:&response
                                                        error:&requestError];

    if (requestError){
        NSLog(@"REQUEST Error for word %@ - %@", mTranslateModel.firstWord, [requestError localizedDescription]);
    }

    if (response.statusCode == 200)
    {
        NSError* parseError = nil;
        TranslateResultModel* transalteResulteModel = [[TranslateResultModel alloc] initWithData:[NSJSONSerialization JSONObjectWithData:result options:kNilOptions error:&parseError]];

        if(!parseError)
        {
            [mTranslatingDelegate translatedWord:transalteResulteModel.englishWord forModel:mTranslateModel];
        }
        else
        {
            NSLog(@"PARSING Error: %@", mComponents.path);
        }
    }
    else
    {
        [mTranslatingDelegate errorTranslate:mTranslateModel];

        NSLog(@"RESPONSE Error: %@ Code: %i", mTranslateModel.firstWord, (int)response.statusCode);
    }
    
    isFinished = YES;
    isExecuting = NO;
}

-(BOOL)isFinished
{
    return isFinished;
}

-(BOOL)isExecuting
{
    return isExecuting;
}

@end
